@include('template.main')
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="/"><strong>Ultramarket</strong></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link" aria-current="page" href="/barang">Barang</a>
                @if (session()->get('role') != 'admin')
                <a class="nav-link" href="/keranjang">Keranjang</a>
                @else
                  <a class="nav-link" href="/supplier">Supplier</a>
                @endif
            </div>
        </div>
        <div class="d-flex align-items-center gap-2">
            <p class="p-0 m-0 text-white text-center mx-2">Halo, {{session()->get('username')}}</p>
            @if (session()->get('role') == 'admin')
            <a href="/admin/{{session()->get('id_akun')}}"><img src="{{asset('img/ic_profile.png')}}" alt=""
              @else
              <a href="/pengguna/{{session()->get('id_akun')}}"><img src="{{asset('img/ic_profile.png')}}" alt=""
                
            @endif
                    width="40px"></a>
            <a href="/logout" class="btn btn-danger">LOGOUT</a>
        </div>
    </div>
</nav>
@yield('container')
@include('template.footer')