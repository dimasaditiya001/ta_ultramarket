<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\pelangganModel;
use App\Models\akunModel;


class penggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Halaman Login',
        ];
        return view('pages/login', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function log(Request $request)
    {
        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])) {
            $id = Auth::user()->id;
            $akun = akunModel::all()->find($id);
            $request->session()->regenerate();
            $request->session()->put('id_akun', $id);
            $request->session()->put('role', $akun['role']);
            $request->session()->put('username', $akun['username']);
            // dd(session()->get('username'));
            return redirect()->intended('/barang');
        } else {
            // dd('login gagal');
            return back()->with('loginError', 'Login Gagal!');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            akunModel::create([
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'role' => 'user',
            ]);
            $akun = akunModel::orderBy('id', 'DESC')->first();
            // dd($akun);php artisan migrate:fresh
            pelangganModel::create([
                'nama_pelanggan' => $request->nama,
                'kontak_pelanggan' => $request->kontak,
                'akun_id' => $akun['id'],
            ]);
            return redirect('/login');
        } catch (\Illuminate\Database\QueryException $ex) {
            return back()->with('unik', 'Username Sudah Dipakai, silahkan pilih username lain');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cust = pelangganModel::all();
        $cust = $cust->find($id);
        $cust = DB::table('pelanggan')
            ->join('akun', 'pelanggan.akun_id', '=', 'akun.id')
            ->where('pelanggan.akun_id', '=', $id)
            ->select('akun.username', 'pelanggan.nama_pelanggan', 'pelanggan.kontak_pelanggan', 'pelanggan.akun_id', 'pelanggan.id')->first();
        // ->get();
        // dd($cust);

        // $col = ['Nama Supplier', 'Kontak Supplier'];
        // $cols = ['nama_supplier', 'kontak_supplier'];
        $data = [
            'title' => 'Profile Pengguna',
            'content' => $cust,
            // 'col' => $col,
            // 'cols' => $cols,
        ];

        // dd($supp);
        return view('/pages/profile', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->input('username'));
        $pelanggan = pelangganModel::all()->find($id);
        $pelanggan->nama_pelanggan = $request->input('nama');
        $pelanggan->kontak_pelanggan = $request->input('kontak');
        $pelanggan->update();

        $user = akunModel::all()->find($request->input('id'));
        // dd($admin);
        $user->username = $request->input('username');
        $user->update();


        return redirect('pengguna/' . $id)->with('success', 'Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
