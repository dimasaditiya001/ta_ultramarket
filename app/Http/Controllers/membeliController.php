<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\membeliModel;
use App\Models\barangModel;
use Illuminate\Support\Facades\DB;

class membeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_akun = session()->get('id_akun');
        $beli = DB::table('membeli')
        ->join('barang', 'membeli.barang_id', '=', 'barang.id')
            ->join('akun', 'membeli.akun_id', '=', 'akun.id')
            ->where('akun_id','=', $id_akun)
            ->select('membeli.*', 'barang.harga_barang', 'barang.stok', 'barang.nama_barang')
            ->get();
        // dd($beli);
        $data = [
            'title' => 'Keranjang',
            'beli' => $beli,
        ];
        return view('/pages/keranjang', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jumlah = $request->jumlah;
        $id_barang = $request->id_barang;
        $barang = barangModel::all()->find($id_barang);
        DB::table('barang')->where('id', $id_barang)->update(['stok' => $barang['stok'] - $jumlah]);

        membeliModel::create([
            'akun_id' => $request->id_akun,
            'barang_id' => $id_barang,
            'jumlah' => $jumlah,
        ]);
        return redirect('/keranjang')->with('success', 'Berhasil dimasukkan ke keranjang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beli = membeliModel::all()->find($id);
        $barang = barangModel::all()->find($beli->barang_id);
        $stok = $barang->stok;
        $barang->stok = $beli->jumlah + $stok;
        $barang->update();

        $beli->delete();
        return redirect('/keranjang')->with('success', 'Terhapus');
    }
}