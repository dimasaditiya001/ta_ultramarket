@extends('template.nav')
@section('container')
<div class="container add">
    <form action="/supplier" method="post">
        @csrf
        <h3 class="my-2">Tambah Supplier</h3>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
            Data Berhasil {{session('success')}}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        <label for="nama" class="form-label">Nama Supplier</label>
        <input type="text" name="nama" class="form-control">
        <label for="kontak" class="form-label mt-2">Kontak Supplier</label>
        <input type="text" name="kontak" class="form-control">
        <input type="submit" value="Tambah Data" class="btn btn-primary mt-2">
    </form>
</div>
<div class="container mt-3 d-flex gap-3">
    @foreach ($supp as $s)
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{$s['nama_supplier']}}</h5>
            <p class="card-text">{{$s['kontak_supplier']}}</p>
            <div class="d-flex justify-content-around">
                <a href="/supplier/edit/{{$s['id']}}" class="btn btn-success">Edit</a>
                <a href="/supplier/hapus/{{$s['id']}}" class="btn btn-danger">Hapus</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection