@extends('template.nav')
@section('container')
<div class="container">
    <h1 class="my-3">Edit Data Supplier</h1>
    <form action="/supplier/update/{{$content['id']}}" method="post">
      @csrf
        <label for="nama" class="form-label">Nama Supplier</label>
        <input type="text" name="nama" class="form-control" value="{{$content['nama_supplier']}}">
        <label for="kontak" class="form-label">Kontak Supplier</label>
        <input type="text" name="kontak" class="form-control" value="{{$content['kontak_supplier']}}">
        <input type="submit" value="Update Data" class="btn btn-primary mt-3">
    </form>
</div>
@endsection
