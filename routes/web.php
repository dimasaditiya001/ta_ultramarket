<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\barangController;
use App\Http\Controllers\supplierController;
use App\Http\Controllers\membeliController;

use App\Http\Controllers\penggunaController;
use App\Http\Controllers\karyawanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//AKUN
Route::get('/login', [penggunaController::class, 'index'])->name('login')->middleware('guest'); //LOGIN
Route::post('/login', [penggunaController::class, 'log']); //LOG
Route::get('/logout', [penggunaController::class, 'logout']); //LOGOUT
Route::get('/register', function () { // HALAMAN REGISTER USER
    return view('pages/register', ['title' => 'Halaman Register Pengguna']);
});

Route::get('/registeradmin', function () { // HALAMAN REGISTER ADMIN
    return view('pages/registeradmin', ['title' => 'Halaman Register Admin']);
});

Route::post('/register', [penggunaController::class,'store']); // ADD USER
Route::post('/registeradmin', [karyawanController::class,'store']); // ADD ADMIN

Route::post('/barang/beli', [membeliController::class, 'store'])->middleware('auth'); // BELI BARANG

Route::get('/', [barangController::class, 'index'])->middleware('auth'); // HALAMAN AWAL 
// END OF AKUN

// BARANG
Route::prefix('barang')->group(function(){ 
    Route::controller(barangController::class)->group(function(){
        Route::group(['middleware' => 'auth'], function(){
            Route::get('/', 'index');
            Route::get('/hapus/{id}', 'destroy');
            Route::get('/edit/{id}', 'edit');
            Route::post('/update/{id}', 'update');
            Route::post('/','store');
        });
    });
});
// END OF BARANG


// KERANJANG
Route::prefix('keranjang')->group(function(){ 
    Route::controller(membeliController::class)->group(function(){
        Route::group(['middleware' => 'auth'], function(){
        Route::get('/', 'index');
        Route::get('/hapus/{id}', 'destroy');
        Route::get('/edit/{id}', 'edit');
        Route::post('/update/{id}', 'update');
        Route::post('/','store');
    });
    });
});
// END OF KERANJANG

// SUPPLIER
Route::prefix('supplier')->group(function () {
    Route::controller(supplierController::class)->group(function () {
        Route::group(['middleware' => 'auth'], function(){
            Route::get('/', 'index');
            Route::post('/', 'store');
            Route::get('/edit/{id}', 'edit');;
            Route::post('/update/{id}', 'update');;
            Route::get('/hapus/{id}', 'destroy');;
        });
    });
});
// END OF SUPPLIER

// CONTROL PENGGUNA 
Route::prefix('pengguna')->group(function () {
    Route::controller(penggunaController::class)->group(function () {
        Route::group(['middleware' => 'auth'], function(){
            Route::get('/{id}', 'edit');
            Route::post('/profile/update/{id}', 'update');
        });
    });
});

// CONTROL ADMIN
Route::prefix('admin')->group(function () {
    Route::controller(karyawanController::class)->group(function () {
        Route::group(['middleware' => 'auth'], function(){
            Route::get('/{id}', 'edit');
            Route::post('/profile/update/{id}', 'update');
        });
    });
});