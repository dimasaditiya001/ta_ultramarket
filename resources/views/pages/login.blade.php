@include('template.main')
{{-- <div class="d-flex flex-column justify-content-center align-items-center"> --}}
  {{-- <div class="container col-6 p-4 border border-2 border-primary rounded-3 my-auto mt-5"> --}}
    <main class="form-signin p-2 col-6 m-auto text-center mt-5">
        <h2>Login</h2>
        <h3><strong>Ultramarket</strong></h3>
        @if (session()->has('loginError'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{session('loginError')}}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <form action="/login" method="post">
          @csrf
          <div class="form-floating">
            <input type="text" class="form-control" id="username" name="username" placeholder="Username">
            <label for="username" class="form-label">Username</label>
          </div>
          <div class="form-floating">
            <input type="password" class="form-control" id="password" name="password" minlength="8" placeholder="Password">
            <label for="password" class="form-label">Password</label>
          </div>
          <div class="d-flex justify-content-around align-items-center mt-4">
            <input type="submit" value="LOGIN" class="btn btn-primary">
            <a href="/register" class="btn btn-success">REGISTER</a>
          </div>
        </form>
      </div>
    </main>
  {{-- </div> --}}
  @include('template.footer')