@extends('template.nav')
@section('container')
@php
$id_akun = session()->get('id_akun');
$role = session()->get('role');
@endphp
@if ($role != 'user')

<div class="container add">
    @if (session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
        Data Berhasil {{session('success')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <h3 class="my-2">Tambah Barang</h3>
    <form action="/barang" method="POST" enctype="multipart/form-data">
        @csrf
        <label for="nama" class="form-label">Nama Barang</label>
        <input type="text" name="nama" class="form-control">
        <label for="harga" class="form-label mt-2">Harga Barang</label>
        <input type="text" name="harga" class="form-control">
        <label for="stok" class="form-label mt-2">Stok Barang</label>
        <input type="number" name="stok" class="form-control">
        <label for="foto" class="form-label">Foto Barang</label>
        <input class="form-control" type="file" id="foto" name="foto">
        <label for="supplier" class="form-label mt-2">Supplier</label>
        <select class="form-select" name="supplier" id="supplier">
            @foreach ($supp as $s)
            <option value="{{$s['id']}}">{{$s['nama_supplier']}}</option>
            @endforeach
        </select>

        <input type="submit" value="Tambah Data" class="btn btn-primary mt-3">
    </form>
</div>
@endif
<div class="container mt-5">
    <h2>Daftar Barang</h2>
    <div class="container mt-3 gap-4 d-flex">
        @foreach ($barang as $b)
        <div class="card" style="width: 18rem;">
            <img src="{{Storage::url('img/'.$b['foto_barang'])}}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">{{$b['nama_barang']}}</h5>
                <p class="card-text">Rp. {{$b['harga_barang']}}</p>
                <p class="card-text">Stok : {{$b['stok']}}</p>
                @if ($role == 'user')
                <form action="/barang/beli" method="post">
                    @csrf
                    <input type="number" name="jumlah" id="jumlah" placeholder="jumlah" class="form-control mb-2" required>
                    <input type="text" name="id_akun" value="{{$id_akun}}" hidden>
                    <input type="text" name="id_barang" value="{{$b['id']}}" hidden>
                    <input type="submit" value="Beli" class="btn-success btn">
                </form>
                @else
                <a href="/barang/edit/{{$b['id']}}" class="btn btn-success">Edit</a>
                <a href="/barang/hapus/{{$b['id']}}" class="btn btn-danger">Hapus</a>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection