<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\barangModel;
use App\Models\supplierModel;
use Illuminate\Support\Facades\Storage;


class barangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $supplierModel = supplierModel::all();
        $foreign = ''; 
        $barangModel = barangModel::all();
        $data = [
            'title' => 'Barang',
            'barang' => $barangModel,
            'supp' => $supplierModel,
        ];
        return view('pages/barang', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $file = $request->file('file');
        $name = Str::random(10);
        $ext = $request->foto->extension();
        $name = $name. '.'.$ext;
        // dd($name);
        $path = $request->file('foto')->storeAs('public/img', $name);

        barangModel::create([
            'nama_barang' => $request->nama,
            'harga_barang' => $request->harga,
            'foto_barang' => $name,
            'stok' => $request->stok,
            'supplier_id' => $request->supplier,
        ]);
        return redirect('/barang')->with('success', 'Disimpan');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supp = supplierModel::all();
        $barang = barangModel::all();
        $barang = $barang->find($id);
        // $col = ['Nama Supplier', 'Kontak Supplier'];
        // $cols = ['nama_supplier', 'kontak_supplier'];
        $data = [
            'title' => 'Barang Edit',
            'content' => $barang,
            'supp' => $supp,
            // 'col' => $col,
            // 'cols' => $cols,
        ];
        
        // dd($supp);
        return view('/pages/edit/barangedit', $data);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        // $supp = supplierModel::all()->find($id)->update($request->all());
        $barang = barangModel::all()->find($id);
        if ($request->file('foto') != null) {
            $fotoLama = $barang->foto_barang;
            Storage::delete('public/img/'.$fotoLama);
            $name = Str::random(10);
            $ext = $request->foto->extension();
            $name = $name . '.' . $ext;
            // dd($name);
            $path = $request->file('foto')->storeAs('public/img', $name);
            $barang->foto_barang = $name;
        }
        $barang->nama_barang = $request->input('nama');
        $barang->harga_barang = $request->input('harga');
        $barang->stok = $request->input('stok');
        $barang->supplier_id = $request->input('supplier');
        $barang->update();
        // $supp->find($id)->update($request->all());
        return redirect('/barang')->with('success', 'Terupdate');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = barangModel::all()->find($id);
        $foto = $barang->foto_barang;
        $barang->delete();
        Storage::delete('public/img/' . $foto);
        return redirect('/barang')->with('success', 'Terhapus');
    }
}
