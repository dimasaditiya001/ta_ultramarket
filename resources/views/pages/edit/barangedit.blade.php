@extends('template.nav')
@section('container')
<div class="container">
    <h1 class="my-3">Edit Data Barang</h1>
    <form action="/barang/update/{{$content['id']}}" method="post" enctype="multipart/form-data">
      @csrf
        <label for="nama" class="form-label">Nama Barang</label>
        <input type="text" name="nama" class="form-control" value="{{$content['nama_barang']}}">
        <label for="harga" class="form-label">Harga Barang</label>
        <input type="text" name="harga" class="form-control" value="{{$content['harga_barang']}}">
        <label for="stok" class="form-label">Harga Barang</label>
        <input type="number" name="stok" class="form-control" value="{{$content['stok']}}">
        <label for="foto" class="form-label">Foto Barang</label>
        <input class="form-control" type="file" id="foto" name="foto" value="{{$content['foto_barang']}}">
        <label for="supplier" class="form-label">Supplier</label>
        <select class="form-select" name="supplier" id="supplier">
          @foreach ($supp as $s)
            <option value="{{$s['id']}}">{{$s['nama_supplier']}}</option>
          @endforeach
        </select>
        <input type="submit" value="Update Data" class="btn btn-primary mt-3">
    </form>
</div>
@endsection