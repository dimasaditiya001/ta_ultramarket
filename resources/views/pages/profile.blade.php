@extends('template.nav')
@section('container')
@php
    $role = true;
    $table = 'karyawan';
    $url = 'admin';
    if(session()->get('role') != 'admin'){
        $role=false;
        $table = 'pelanggan';  
        $url = 'pengguna';
    }
    $nama = 'nama_'.$table;
    $kontak = 'kontak_'.$table;
@endphp
<div class="container">
    <h2 class="text-center mt-4">Profile {{ucwords($url)}}</h2>
    @if (session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
        Data Berhasil {{session('success')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <form action="/{{$url}}/profile/update/{{$content->id}}" method="post">
        @csrf
        <input type="text" name="id" class="form-control" hidden disabled value="{{$content->akun_id}}">
        <label for="username" class="form-label">Username</label>
        <input id="username" type="text" name="username" class="form-control" disabled value="{{$content->username}}">
        <label for="nama" class="form-label">Nama</label>
        <input type="text" name="nama" class="form-control" disabled value="{{$content->$nama}}">
        <label for="kontak" class="form-label">Kontak</label>
        <input type="text" name="kontak" class="form-control" disabled value="{{$content->$kontak}}">
        <button id="btn-edit" type="button" class="btn btn-primary mt-3" onclick="profileBtn()">Edit</button>
        <div class="d-flex gap-5 mt-3 align-items-center mt-3">
            <input type="submit" id="btn-submit" hidden value="Simpan Data" class="btn btn-success">
            <a href="/{{$url}}/{{$content->id}}" hidden id="btn-batal" class="btn btn-danger text-center">Batal</a>
        </div>
    </form>
</div>
@endsection