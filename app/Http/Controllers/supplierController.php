<?php

namespace App\Http\Controllers;

use App\Models\supplierModel;

use Illuminate\Http\Request;

class supplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Supplier',
            'supp' => supplierModel::all(),
        ];
        return view('pages/supplier', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplierModel = new supplierModel;
        supplierModel::create([
            'nama_supplier' => $request->nama,
            'kontak_supplier' => $request->kontak
        ]);
        // $supplierModel->save();
        return redirect('/supplier')->with('success', 'Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supp = supplierModel::all();
        $supp = $supp->find($id);
        $data = [
            'title' => 'Supplier Edit',
            'content' => $supp,
        ];

        // dd($supp);
        return view('/pages/edit/supplieredit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        // $supp = supplierModel::all()->find($id)->update($request->all());
        $supp = supplierModel::all()->find($id);
        $supp->nama_supplier = $request->input('nama');
        $supp->kontak_supplier = $request->input('kontak');
        $supp->update();
        // $supp->find($id)->update($request->all());
        return redirect('/supplier')->with('success', 'Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supp = supplierModel::all();
        $supp->find($id)->delete();
        return redirect('/supplier')->with('success', 'Dihapus');
    }
}
